use semver::{Prerelease, VersionReq};

fn main() {
    let header_version = build_probe_petsc::probe("3.16").get_version_from_headers();

    // we assume that there is only one version of pre-release,
    // which is currently the case for PETSc.
    let header_version_without_pre = {
        let mut hv = header_version.clone();
        hv.pre = Prerelease::EMPTY;
        hv
    };

    // We remove the pre-release because it makes things hard to compare
    let petsc_version_3 = VersionReq::parse("^3.16")
        .unwrap()
        .matches(&header_version_without_pre);
    let petsc_version_ge_4 = VersionReq::parse(">=4")
        .unwrap()
        .matches(&header_version_without_pre);

    if petsc_version_3 {
        println!("cargo:rustc-cfg=petsc_version_3");
    } else if petsc_version_ge_4 {
        println!("cargo:rustc-cfg=petsc_version_ge_4");
        panic!("petsc-rs doesn't currently support PETSc v4 or above.")
    }
}
